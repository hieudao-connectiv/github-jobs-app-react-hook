import { useReducer, useEffect } from "react";
import axios from 'axios';


axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

const ACTIONS = {
  MAKE_REQUEST: 'make-request',
  GET_DATA: 'get-data',
	ERROR: 'error',
	UPDATE_HAS_NEXT_PAGE: 'update-has-next-page'
}


// https://stackoverflow.com/questions/29670703/how-to-use-cors-anywhere-to-reverse-proxy-and-add-cors-headers
// https://cors-anywhere.herokuapp.com/: This API enables cross-origin requests to anywhere
// CORS Anywhere will then make the request on behalf of your application, and add CORS headers to the response so that your web application can process the response.
const BASE_URL = "https://cors-anywhere.herokuapp.com/https://jobs.github.com/positions.json";
// const BASE_URL = "https://jobs.github.com/positions.json";



// the reducer() function will be automatically called (to excute the dispatch() with its parameters) when the dispatch() function is called
function reducer(state, action) {
	// console.log("2-1 action", action);

  switch (action.type) {
    case ACTIONS.MAKE_REQUEST:
			return { loading: true, jobs: []}

    case ACTIONS.GET_DATA:
			return { ...state, loading: false, jobs: action.payload.jobs}

    case ACTIONS.ERROR:
			return {...state, loading: false, error: action.payload.error, jobs: []}

		case ACTIONS.UPDATE_HAS_NEXT_PAGE: 
			return {...state, hasNextPage: action.payload.hasNextPage}

    default: 
      return state
  }
}

export default function useFetchJobs(params, page) {

  // whatever we pass to it is populated inside of this action variable 
  const [state, dispatch] = useReducer(reducer, { jobs: [], loading: true });

	// useEffect will run after each render and update
  useEffect(() => {

		const cancelToken1 = axios.CancelToken.source()

		dispatch({type: ACTIONS.MAKE_REQUEST});
		// console.log("1-2 state: ", state);


    axios.get(BASE_URL, {
			cancelToken: cancelToken1.token,

			// https://jobs.github.com/api
			// parameters: markdown - set to 'true' to get the description and how_to_apply fields as Markdown
			params: { markdown: true, page: page, ...params},
    }).then(res => {
			// console.log(res.data)
			dispatch ({ type: ACTIONS.GET_DATA, payload: { jobs: res.data } })
    }).catch(e => { 
			if (axios.isCancel(e)) return;
      dispatch ({ type: ACTIONS.ERROR, payload: { error: e} })
		})

		const cancelToken2 = axios.CancelToken.source()
		axios.get(BASE_URL, {
			cancelToken: cancelToken2.token,
			params: { markdown: true, page: page + 1, ...params},
    }).then(res => {
			dispatch ({ type: ACTIONS.UPDATE_HAS_NEXT_PAGE, payload: { hasNextPage: res.data.length !== 0 } })
    }).catch(e => { 
			if (axios.isCancel(e)) return;
      dispatch ({ type: ACTIONS.ERROR, payload: { error: e} })
		})


		console.log(params);
		console.log(page);


		// 'cleanup' function to clean up our code above (in useEffect) or Let's cancel the request on effect cleanup (stop Axios in its tracks.)
		return () => {
			cancelToken1.cancel();
			cancelToken2.cancel();
		}

  }, [params, page])

  return state;
}